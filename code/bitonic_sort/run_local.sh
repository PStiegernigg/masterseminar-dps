#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
cd "$SCRIPT_DIR"

BIN_DIR="./bin/"
OUTPUT_FILE="./analyse.csv"

execute_parallel_task_5_times(){
  for count in {0..4} ; do
    for numThreads in {1,2,4,8} ; do
      echo "run: $1 $2 $numThreads"
      $BIN_DIR/$1 $2 $numThreads >> "$1".csv
    done
  done
}

execute_parallel_task_5_times_mpi(){
  for count in {0..4} ; do
    for numThreads in {1,2,4,8} ; do
      echo "run: $1 $2 $numThreads"
      mpirun --oversubscribe -n $numThreads $BIN_DIR/$1 $2 >> "$1".csv
    done
  done
}

for size in {65536,131072,262144,} ; do
    execute_parallel_task_5_times bitonic_sort $size
    execute_parallel_task_5_times bitonic_sort_omp $size
    execute_parallel_task_5_times bitonic_sort_omp_dist $size
    execute_parallel_task_5_times_mpi bitonic_sort_mpi $size
done
