/*
 * Sorting of N-element array using odd-even transposition sort.
 */
#include "util.h"
#include <mpi.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#define MPI_MASTER 0
#define ASCENDING  true
#define DESCENDING false

void bitonic_merge(int* lo, unsigned n, bool dir) {
        if(n > 1) {
            int m=n/2;

            for (int i = 0; i <  m; i++) {
              compare_exchange(lo + i, lo + i + m, dir);
            }

            bitonic_merge(lo, m, dir);
            bitonic_merge(lo+m, m, dir);
        }
    }

void bitonic_sort(int* lo, unsigned n, bool dir) {
       if (n > 1) {
           int m=n/2;
           bitonic_sort(lo, m, ASCENDING);
           bitonic_sort(lo + m, m, DESCENDING);
           bitonic_merge(lo, n, dir);
       }
}

/*
 * Sorting algorithm.
 */
void bitonic_sort_serial(int *a, unsigned n) {
  bitonic_sort(a, n, ASCENDING);
}

void keep_lower(int *array1, int *array2, int size) {
  int *destination = array1;
  int *lower = (int *)malloc(size * sizeof(int));

  for (int i = 0; i < size; ++i) {
    if (*array1 <= *array2) {
      lower[i] = *array1;
      ++array1;
    } else {
      lower[i] = *array2;
      ++array2;
    }
  }

  memcpy(destination, lower, size * sizeof(int));
  free(lower);
}

void keep_higher(int *array1, int *array2, int size) {
  int *destination = array1;
  array1 = array1 + (size - 1);
  array2 = array2 + (size - 1);

  int *higher = (int *)malloc(size * sizeof(int));

  for (int i = size - 1; i >= 0; --i) {
    if (*array1 >= *array2) {
      higher[i] = *array1;
      --array1;
    } else {
      higher[i] = *array2;
      --array2;
    }
  }

  memcpy(destination, higher, size * sizeof(int));
  free(higher);
}

void compare_low(int *sendBuf, int *recvBuf, int size, int rank) {
  MPI_Send(sendBuf, size, MPI_INT, rank, 0, MPI_COMM_WORLD);
  MPI_Recv(recvBuf, size, MPI_INT, rank, 0, MPI_COMM_WORLD,
           MPI_STATUS_IGNORE);
  keep_lower(sendBuf, recvBuf, size);
}

void compare_high(int *sendBuf, int *recvBuf, int size, int rank) {
  MPI_Recv(recvBuf, size, MPI_INT, rank, 0, MPI_COMM_WORLD,
           MPI_STATUS_IGNORE);
  MPI_Send(sendBuf, size, MPI_INT, rank, 0, MPI_COMM_WORLD);
  keep_higher(sendBuf, recvBuf, size);
}

/*
 * Main program.
 */
int main(int argc, char **argv) {
  if (argc < 2) {
    fprintf(stderr, "Usage: %s <problem-size>\n", argv[0]);
    return EXIT_FAILURE;
  }

  unsigned problem_size = atoi(argv[1]);
  int *array, *comp;

  MPI_Init(&argc, &argv);

  int rank, num_proc, size_per_proc;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

  size_per_proc = problem_size / num_proc;
  struct timespec start, end;

  if (rank == MPI_MASTER) {

    if (problem_size % num_proc != 0) {
      fprintf(stderr, "Problem size not evenly divisible.\n");
      MPI_Abort(MPI_COMM_WORLD, 1);
    }

    if(!is_power_of_2(num_proc)) {
        fprintf(stderr, "Process number must be a power of 2.\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    array = (int *)malloc(problem_size * sizeof(int));
    comp = (int *)malloc(problem_size * sizeof(int));
    fill_randomly(array, comp, problem_size);
    clock_gettime(CLOCK_MONOTONIC, &start);
  }

  else {
    array = (int *)malloc(size_per_proc * sizeof(int));
  }

  MPI_Scatter(array, size_per_proc, MPI_INT, array, size_per_proc, MPI_INT,
              MPI_MASTER, MPI_COMM_WORLD);

  // Sort received subarray sequentially
  bitonic_sort_serial(array, size_per_proc);

  int *buffer = (int *)malloc(size_per_proc * sizeof(int));

  int id, target_rank, dim = ((unsigned) log2(num_proc));
  bool j_bit;

  for(int i = 1; i <= dim; i++) {
    id = rank >> i;
    for(int j = i-1; j >= 0; j--) {
      j_bit = rank & (1 << j);
      target_rank = rank ^ (1 << j);

      if(((id % 2 == 0) && !j_bit) || ((id % 2 != 0) && j_bit))  {
        compare_low(array, buffer, size_per_proc, target_rank);
      }
      else {
        compare_high(array, buffer, size_per_proc, target_rank);
      }
    }
  }

  MPI_Gather(array, size_per_proc, MPI_INT, array, size_per_proc, MPI_INT,
             MPI_MASTER, MPI_COMM_WORLD);

  if (rank == MPI_MASTER) {
    clock_gettime(CLOCK_MONOTONIC, &end);

    double execTime = (end.tv_sec - start.tv_sec);
    execTime += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

    // print as csv
    // algo | execTime [s] | problemSize | numOfUsedThreads | comment
    printf("Bitonic_Sort_MPI,%.3f,%d,%d,Sorted-correctly: %s\n", execTime,
           problem_size, num_proc, "Unknown");
           // checkSorting(comp, array, problem_size) ? "TRUE" : "FALSE");
  }

  free(array);
  free(buffer);

  MPI_Finalize();
  return EXIT_SUCCESS;
}
