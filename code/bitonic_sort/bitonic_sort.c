/*
 * Sorting of N-element array using bitonic sort
 */
#include <omp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "util.h"

#define ASCENDING  true
#define DESCENDING false

void bitonic_merge(int* lo, unsigned n, bool dir) {
        if(n > 1) {
            int m=n/2;
            for (int i = 0; i <  m; i++) {
              compare_exchange(lo + i, lo + i + m, dir);
            }
            bitonic_merge(lo, m, dir);
            bitonic_merge(lo+m, m, dir);
        }
}

void bitonic_sort(int* lo, unsigned n, bool dir) {
       if (n > 1) {
           int m=n/2;
           bitonic_sort(lo, m, ASCENDING);
           bitonic_sort(lo + m, m, DESCENDING);
           bitonic_merge(lo, n, dir);
       }
}

/*
 * Sorting algorithm.
 */
void bitonic_sort_serial(int *a, unsigned n) {
  bitonic_sort(a, n, ASCENDING);
}

/*
 * Main program.
 */
int main(int argc, char **argv) {
  if (argc < 3) {
    fprintf(stderr, "Usage: %s <problem-size> <num-of-used-threads>\n",
            argv[0]);
    return EXIT_FAILURE;
  }

  unsigned problemSize = atoi(argv[1]);

  if(!is_power_of_2(problemSize)) {
      fprintf(stderr, "Problemsize must be a power of 2.\n");
      return EXIT_FAILURE;
  }


  unsigned numOfUsedThreads = atoi(argv[2]);
  omp_set_num_threads(numOfUsedThreads);

  srand(time(NULL));

  int* array = (int*) malloc(problemSize * sizeof(int));
  //int* comp = (int*) malloc(problemSize * sizeof(int));

  unsigned i;
  for (i = 0; i < problemSize; ++i) {
    array[i] =
        rand() % (10 * problemSize); // range restricted to improve output
    //comp[i] = array[i];
  }

  struct timespec start, end;

  clock_gettime(CLOCK_MONOTONIC, &start);
  bitonic_sort_serial(array, problemSize);
  clock_gettime(CLOCK_MONOTONIC, &end);

  double execTime = (end.tv_sec - start.tv_sec);
  execTime += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

  // print as csv
  // algo | execTime [s] | problemSize | numOfUsedThreads | comment
  printf("Bitonic_Sort_Serial,%.3f,%d,%d,Sorted-correctly: %s\n", execTime,
         problemSize, numOfUsedThreads, "Unknown");
         // checkSorting(comp, array, problemSize) ? "TRUE" : "FALSE");

  return EXIT_SUCCESS;
}
