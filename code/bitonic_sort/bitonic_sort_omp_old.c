/*
 * Sorting of N-element array using bitonic sort
 */
#include <omp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define ASCENDING  true
#define DESCENDING false

void compare_exchange(int *lhs, int *rhs, bool dir) {
  if(dir == (*lhs > *rhs)) {
    int temp = *lhs;

    *lhs = *rhs;
    *rhs = temp;
  }
}

void bitonic_merge(int* lo, unsigned n, bool dir) {
        if(n > 1) {
            int m=n/2;

            for (int i = 0; i <  m; i++) {
              compare_exchange(lo + i, lo + i + m, dir);
            }

            bitonic_merge(lo, m, dir);
            bitonic_merge(lo+m, m, dir);
        }
}

void bitonic_sort(int* lo, unsigned n, bool dir) {
       if (n > 1) {
          int m=n/2;

          #pragma omp parallel
          {
            #pragma omp single nowait
            {
              #pragma omp task
              bitonic_sort(lo, m, ASCENDING);

              #pragma omp task
              bitonic_sort(lo + m, m, DESCENDING);

              #pragma omp taskwait
              bitonic_merge(lo, n, dir);
            }
          }

       }
}

/*
 * Sorting algorithm.
 */
void bitonic_sort_omp(int *a, unsigned n) {
  bitonic_sort(a, n, ASCENDING);
}

void printArray(int *a, int n) {
  int i;
  for (i = 0; i < n; i++) {
    printf("%3d ", a[i]);
  }

  putchar('\n');
}

int checkSorting(int *should, int *is, int problemSize) {
  // Check if array is sorted in ascending order.
  int i;
  for (i = 1; i < problemSize; i++)
    if (is[i - 1] > is[i])
      return 0;

  // Check if sorted array contains all elements of the original array.
  // Because we checked previously if the array is sorted, same elements are
  // located together.
  for (i = 0; i < problemSize; i++) {
    int count = 1;

    // Count ocurrences of current element in sorted array.
    int j;
    for (j = i; j < problemSize - 1; i++, j++)
      if (is[j] == is[j + 1])
        count++;
      else
        break;

    // Check if the current element is[i] occurs in the original array
    // just as often as counted above.
    for (j = 0; j < problemSize; j++)
      if (should[j] == is[i])
        count--;

    // 'count' has to be 0 at that point, otherwise at least one element
    // is missing or appears too often in the sorted array.
    if (count != 0)
      return 0;
  }

  return 1;
}

bool is_power_of_2(unsigned x) {
   return x && !(x & (x - 1));
}

/*
 * Main program.
 */
int main(int argc, char **argv) {
  if (argc < 3) {
    fprintf(stderr, "Usage: %s <problem-size> <num-of-used-threads>\n",
            argv[0]);
    return EXIT_FAILURE;
  }

  unsigned problemSize = atoi(argv[1]);

  if(!is_power_of_2(problemSize)) {
      fprintf(stderr, "Problemsize must be a power of 2.\n");
      return EXIT_FAILURE;
  }


  unsigned numOfUsedThreads = atoi(argv[2]);
  omp_set_num_threads(numOfUsedThreads);
  omp_set_nested(true);
  omp_set_max_active_levels(((int) log2(numOfUsedThreads)));

  srand(time(NULL));

  int* array = (int*) malloc(problemSize * sizeof(int));
  //int* comp = (int*) malloc(problemSize * sizeof(int));

  unsigned i;
  for (i = 0; i < problemSize; ++i) {
    array[i] =
        rand() % (10 * problemSize); // range restricted to improve output
    //comp[i] = array[i];
  }

  struct timespec start, end;

  clock_gettime(CLOCK_MONOTONIC, &start);
  bitonic_sort_omp(array, problemSize);
  clock_gettime(CLOCK_MONOTONIC, &end);

  double execTime = (end.tv_sec - start.tv_sec);
  execTime += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

  // print as csv
  // algo | execTime [s] | problemSize | numOfUsedThreads | comment
  printf("bitonic_sort_omp,%.3f,%d,%d,Sorted-correctly: %s\n", execTime,
         problemSize, numOfUsedThreads, "Unknown");
         // checkSorting(comp, array, problemSize) ? "TRUE" : "FALSE");

  return EXIT_SUCCESS;
}
