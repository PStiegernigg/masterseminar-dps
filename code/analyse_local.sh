#!/bin/bash

if [ "$#" -eq 0 ] ; then
  echo "Usage: $0 <hw-folder>"
  echo "Example: $0 hw1"
  exit 1
fi

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
cd "$SCRIPT_DIR"

cat "$1/analyse.csv" | grep -e '^.*,.*,.*,.*,.*$' | ./analyse.py ${@:2}
