#!/usr/bin/env python3

import collections
import csv
import getopt
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import subprocess
import sys

measurments = collections.OrderedDict()

def usage():
    print('usage: ', sys.argv[0], ' [-i <csv-file>] [-t json] [-o <output-pdf>]')

def analyse(input_stream):
    reader = csv.reader(input_stream)
    for row in reader:
        algo = collections.OrderedDict(measurments.get(row[0], {}))
        problem = collections.OrderedDict(algo.get(row[2], {}))
        thread = problem.get(row[3], {})
        thread['sum'] = thread.get('sum', 0.0) + float(row[1])
        thread['count'] = thread.get('count', 0) + 1
        problem[row[3]] = thread
        algo[row[2]] = problem
        measurments[row[0]] = algo

    for _,algo in measurments.items():
        for _,problem in algo.items():
            for _,thread in problem.items():
                thread['avg'] = thread['sum'] / thread['count']

    for _,algo in measurments.items():
        for _,problem in algo.items():
            for cores,thread in problem.items():
                thread['speedup'] =  round(problem['1']['avg'] / thread['avg'], 3)
                thread['efficiency'] = round(thread['speedup'] / int(cores), 3)

    for _,algo in measurments.items():
        for _,problem in algo.items():
            for cores,thread in problem.items():
                thread['avg'] = round(thread['avg'], 3)
                thread['sum'] = round(thread['sum'], 3)

def dot_plot(out_file_name):
    colors = ['ro', 'go', 'bo']
    next_color = 0
    legend = []
    max_x = 0;
    max_y = 0;
    for algo_name,algo in measurments.items():
        for problem_size,problem in algo.items():
            avgs = []
            threads = []
            for cores,thread in problem.items():
                avgs.append(float(thread['avg']))
                threads.append(int(cores))
                max_x = max(max_x, int(cores))
                max_y = max(max_y, float(thread['avg']))
            plt.plot(threads, avgs, marker='x')
            legend.append(algo_name + " - " + problem_size)

    plt.legend(legend)
    # plt.legend(legend,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.xlabel('Number of threads')
    plt.ylabel('Time in seconds')

    plt.axis([0, int(max_x * 1.2), 0, int(math.ceil(max_y * 1.05))])

    if not out_file_name:
        plt.show()
    else:
        plt.savefig(out_file_name, format='eps', bbox_inches='tight')

def store_json(out_file_name):
    with open(out_file_name, 'w') as outfile:
        json.dump(measurments, outfile)

def print_json():
    print(json.dumps(measurments, indent=2))

def process_input(in_file_name):
    if not in_file_name:
        analyse(sys.stdin);
    else:
        with open(in_file_name) as csv_input:
            analyse(csv_input)

def check_outfile_name(filename):
    if not filename:
        usage()
        sys.exit(1)

def cli(argv):
    inputfile = ''
    outputfile = ''
    output_type = ''

    try:
        opts, args = getopt.getopt(argv,"hi:o:t:",["ifile=","ofile=","type="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-t", "--type"):
            output_type = arg;

    process_input(inputfile)

    if output_type == 'dot':
        dot_plot(outputfile)
    elif output_type == 'json':
        check_outfile_name(outputfile)
        store_json(outputfile)
    else:
        print_json()

if __name__ == "__main__":
    cli(sys.argv[1:])
