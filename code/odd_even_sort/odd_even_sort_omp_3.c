/*
 * Sorting of N-element array using bubble sort.
 */
#include <omp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

bool compare_exchange(int *lhs, int *rhs) {
  if (*lhs > *rhs) {
    int temp = *lhs;

    *lhs = *rhs;
    *rhs = temp;
    return true;
  }

  return false;
}

/*
 * Sorting algorithm.
 */
void odd_even_sort_omp(int *a, unsigned n) {
  bool exchange = true;

#pragma omp parallel
  {
    while (exchange) {
#pragma omp barrier
      exchange = false;
#pragma omp for
      for (unsigned i = 0; i < n - 1; i += 2) {
        if (compare_exchange(&a[i], &a[i + 1]))
          exchange = true;
      }
#pragma omp for
      for (unsigned i = 1; i < n - 1; i += 2) {
        if (compare_exchange(&a[i], &a[i + 1]))
          exchange = true;
      }
    }
  }
}

void printArray(int *a, int n) {
  int i;
  for (i = 0; i < n; i++) {
    printf("%3d ", a[i]);
  }

  putchar('\n');
}

int checkSorting(int *should, int *is, int problemSize) {
  // Check if array is sorted in ascending order.
  int i;
  for (i = 1; i < problemSize; i++)
    if (is[i - 1] > is[i])
      return 0;

  // Check if sorted array contains all elements of the original array.
  // Because we checked previously if the array is sorted, same elements are
  // located together.
  for (i = 0; i < problemSize; i++) {
    int count = 1;

    // Count ocurrences of current element in sorted array.
    int j;
    for (j = i; j < problemSize - 1; i++, j++)
      if (is[j] == is[j + 1])
        count++;
      else
        break;

    // Check if the current element is[i] occurs in the original array
    // just as often as counted above.
    for (j = 0; j < problemSize; j++)
      if (should[j] == is[i])
        count--;

    // 'count' has to be 0 at that point, otherwise at least one element
    // is missing or appears too often in the sorted array.
    if (count != 0)
      return 0;
  }

  return 1;
}

/*
 * Main program.
 */
int main(int argc, char **argv) {
  if (argc < 3) {
    fprintf(stderr, "Usage: %s <problem-size> <num-of-used-threads>\n",
            argv[0]);
    return EXIT_FAILURE;
  }

  unsigned problemSize = atoi(argv[1]);
  unsigned numOfUsedThreads = atoi(argv[2]);
  omp_set_num_threads(numOfUsedThreads);

  srand(time(NULL));

  int* array = (int *)malloc(problemSize * sizeof(int));
  //int* comp = (int *)malloc(problemSize * sizeof(int));

  unsigned i;
  for (i = 0; i < problemSize; ++i) {
    array[i] =
        rand() % (10 * problemSize); // range restricted to improve output
    //comp[i] = array[i];
  }

  struct timespec start, end;

  clock_gettime(CLOCK_MONOTONIC, &start);
  odd_even_sort_omp(array, problemSize);
  clock_gettime(CLOCK_MONOTONIC, &end);

  double execTime = (end.tv_sec - start.tv_sec);
  execTime += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

  // print as csv
  // algo | execTime [s] | problemSize | numOfUsedThreads | comment
  printf("Odd_Even_Sort_OMP,%.3f,%d,%d,Sorted-correctly: %s\n", execTime,
         problemSize, numOfUsedThreads,"Unknown");
         //checkSorting(comp, array, problemSize) ? "TRUE" : "FALSE");

  return EXIT_SUCCESS;
}
