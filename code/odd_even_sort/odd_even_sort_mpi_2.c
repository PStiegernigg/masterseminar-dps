/*
 * Sorting of N-element array using odd-even transposition sort.
 */
#include "util.h"
#include <mpi.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MPI_MASTER 0

/*
 * Sorting algorithm.
 */
void odd_even_sort(int *array, unsigned size) {
  bool exchange = true;

  while (exchange) {
    exchange = false;

    for (unsigned i = 0; i < size - 1; i += 2) {
      if (compare_exchange(&array[i], &array[i + 1])) {
        exchange = true;
      }
    }

    for (unsigned i = 1; i < size - 1; i += 2) {
      if (compare_exchange(&array[i], &array[i + 1])) {
        exchange = true;
      }
    }
  }
}

void keep_lower(int *array1, int *array2, int size) {
  int *destination = array1;
  int *lower = (int *)malloc(size * sizeof(int));

  for (int i = 0; i < size; ++i) {
    if (*array1 <= *array2) {
      lower[i] = *array1;
      ++array1;
    } else {
      lower[i] = *array2;
      ++array2;
    }
  }

  memcpy(destination, lower, size * sizeof(int));
  free(lower);
}

void keep_higher(int *array1, int *array2, int size) {
  int *destination = array1;
  array1 = array1 + (size - 1);
  array2 = array2 + (size - 1);

  int *higher = (int *)malloc(size * sizeof(int));

  for (int i = size - 1; i >= 0; --i) {
    if (*array1 >= *array2) {
      higher[i] = *array1;
      --array1;
    } else {
      higher[i] = *array2;
      --array2;
    }
  }

  memcpy(destination, higher, size * sizeof(int));
  free(higher);
}

void exchange_with_next(int *sendBuf, int *recvBuf, int size, int rank) {
  MPI_Send(sendBuf, size, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
  MPI_Recv(recvBuf, size, MPI_INT, rank + 1, 0, MPI_COMM_WORLD,
           MPI_STATUS_IGNORE);
  keep_lower(sendBuf, recvBuf, size);
}

void exchange_with_previous(int *sendBuf, int *recvBuf, int size, int rank) {
  MPI_Recv(recvBuf, size, MPI_INT, rank - 1, 0, MPI_COMM_WORLD,
           MPI_STATUS_IGNORE);
  MPI_Send(sendBuf, size, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
  keep_higher(sendBuf, recvBuf, size);
}

/*
 * Main program.
 */
int main(int argc, char **argv) {
  if (argc < 2) {
    fprintf(stderr, "Usage: %s <problem-size>\n", argv[0]);
    return EXIT_FAILURE;
  }

  unsigned problem_size = atoi(argv[1]);
  int *array, *comp;

  MPI_Init(&argc, &argv);

  int rank, num_proc, size_per_proc;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

  size_per_proc = problem_size / num_proc;
  struct timespec start, end;

  if (rank == MPI_MASTER) {
    if (problem_size % num_proc != 0) {
      fprintf(stderr, "Problem size not evenly divisible.\n");
      MPI_Abort(MPI_COMM_WORLD, 1);
    }

    array = (int *)malloc(problem_size * sizeof(int));
    comp = (int *)malloc(problem_size * sizeof(int));
    fill_randomly(array, comp, problem_size);
    clock_gettime(CLOCK_MONOTONIC, &start);
  }

  else {
    array = (int *)malloc(size_per_proc * sizeof(int));
  }

  MPI_Scatter(array, size_per_proc, MPI_INT, array, size_per_proc, MPI_INT,
              MPI_MASTER, MPI_COMM_WORLD);

  // Sort received subarray sequentially
  odd_even_sort(array, size_per_proc);

  int *buffer = (int *)malloc(size_per_proc * sizeof(int));

  for (int i = 0; i < num_proc; ++i) {
    if (i % 2 == 0) {      // Even phase
      if (rank % 2 == 0) { // Even host
        if (rank < num_proc - 1)
          exchange_with_next(array, buffer, size_per_proc, rank);
      } else { // Odd host
        if (rank > 0)
          exchange_with_previous(array, buffer, size_per_proc, rank);
      }
    } else {               // Odd phase
      if (rank % 2 == 0) { // Even host
        if (rank > 0)
          exchange_with_previous(array, buffer, size_per_proc, rank);
      } else { // Odd host
        if (rank < num_proc - 1)
          exchange_with_next(array, buffer, size_per_proc, rank);
      }
    }
  }

  MPI_Gather(array, size_per_proc, MPI_INT, array, size_per_proc, MPI_INT,
             MPI_MASTER, MPI_COMM_WORLD);

  if (rank == MPI_MASTER) {
    clock_gettime(CLOCK_MONOTONIC, &end);

    double execTime = (end.tv_sec - start.tv_sec);
    execTime += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

    // print as csv
    // algo | execTime [s] | problemSize | numOfUsedThreads | comment
    printf("Odd_Even_Sort_MPI,%.3f,%d,%d,Sorted-correctly: %s\n", execTime,
           problem_size, num_proc,"Unknown");
           //checkSorting(comp, array, problem_size) ? "TRUE" : "FALSE");
  }

  free(array);
  free(buffer);

  MPI_Finalize();
  return EXIT_SUCCESS;
}
