cmake_minimum_required(VERSION 2.8)

project(odd_even_sort C)

find_package(MPI REQUIRED)
include_directories(${MPI_INCLUDE_PATH})

file(GLOB_RECURSE odd_even_sort_srcs *.c)

set(CMAKE_C_FLAGS "-std=gnu99 -O2 -lrt")

set(all_srcs ${all_srcs} ${odd_even_sort_srcs} PARENT_SCOPE)

set(EXECUTABLE_OUTPUT_PATH bin)

add_executable(odd_even_sort odd_even_sort.c)
target_compile_options(odd_even_sort PRIVATE -fopenmp)
target_link_libraries(odd_even_sort gomp)

add_executable(odd_even_sort_omp odd_even_sort_omp_3.c)
target_compile_options(odd_even_sort_omp PRIVATE -fopenmp)
target_link_libraries(odd_even_sort_omp gomp)

add_executable(odd_even_sort_omp_dist odd_even_sort_omp_dist.c)
target_compile_options(odd_even_sort_omp_dist PRIVATE -fopenmp)
target_link_libraries(odd_even_sort_omp_dist gomp)

add_executable(odd_even_sort_mpi odd_even_sort_mpi_2.c)
target_link_libraries(odd_even_sort_mpi ${MPI_LIBRARIES})

if(MPI_COMPILE_FLAGS)
  set_target_properties(odd_even_sort_mpi PROPERTIES COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif()

if(MPI_LINK_FLAGS)
  set_target_properties(odd_even_sort_mpi PROPERTIES LINK_FLAGS "${MPI_LINK_FLAGS}")
endif()


