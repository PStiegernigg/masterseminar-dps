%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DBIS Seminar Template
%% based on the Springer LNCS style
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[a4paper]{llncs}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{cite}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{float}
\usepackage{amsmath}
\usepackage{placeins}

\graphicspath{{./images/}}

\title{A comparison of OpenMP and MPI implementations for sorting algorithms}
\author{Philipp Stiegernigg}
\institute{}

\begin{document}
\maketitle

\begin{abstract}
Sorting algorithms are one of the most essential algorithms in computer science
and are used in a wide range of applications. With the steady increase of importance
of multi-core processors and distributed systems it is important to determine
how and which sorting algorithms can be efficiently parallelized. This paper
compares parallel implementations of two fundamentally different sorting algorithms.
For this purpose \textit{odd-even-transposition} sort and \textit{bitonic} sort
have been chosen due to their properties. Each of the algorithms is
implemented by using different parallelization techniques with \textit{OpenMP}
and \textit{MPI}. The implementations are evaluated regarding their performance
metrics and memory consumption.

\bigskip
\textit{Keywords} - Sorting Algorithm; Parallel Programming; OpenMP; MPI
\end{abstract}

\FloatBarrier
\section{Introduction}
\label{sec:introduction}

In order to increase performance and achieve higher throughput rates today's
computer architectures changed significantly over the last recent years. Since
it is no longer feasible to increase the CPU clock-frequency by a large amount
, due to power consumption and the resulting heat dissipation, multi and many-core
architectures have become the norm even for small devices like smartphones.
For this reason many parallel algorithms have been proposed in the past to
efficiently utilize this new kind of hardware. One of the most important classes
of algorithms are sorting-algorithms. They are the foundation of a wide range
of basic tasks in computer science. It is therefore essential to choose the best
sorting algorithm and parallelization strategy for the target system. However
this can be a challenging task for software developers.

In this paper different sorting algorithms are implemented using different
parallel techniques and evaluated for their performance. For this purpose
two fundamentally different sorting algorithms have been selected. The
\textit{odd-even-transposition sort} is the simplest known parallel
sorting algorithm and is a good base reference. In contrast to this
\textit{bitonic sort} is one of the best performing sorting algorithms based
on comparator networks. Both algorithms have been implemented using \textit{OpenMP}
\cite{openmp08} and \textit{MPI} \cite{mpi12}.

The rest of the paper is organized as follows: In section \ref{sec:relatedWork}
an overview of related works is given and describes in which aspects they
differ from this paper. Section \ref{sec:background} provides all the necessary background
information to understand the chosen sorting algorithms their sequential execution and
which parts of the algorithms can potentially be run in parallel.
In section \ref{sec:implementation} detailed explanations are given how the
\textit{OpenMP} and \textit{MPI} versions of the algorithms were implemented.
Section \ref{sec:evaluation} shows the gathered performance results and memory
consumption of the performed experiments. Finally section \ref{sec:conclusion}
gives a summary of the conducted work and achieved results.

\FloatBarrier
\section{Related Work}
\label{sec:relatedWork}

\textit{Bitonic sort} was first introduced by K. E. Batcher in the technical report
\textit{Bitonic sorting} \cite{batcher64} and later popularized by his publication
\textit{Sorting networks and their applications} \cite{batcher68}.
The \textit{odd-even-transposition sort} algorithm was explained for the first time
and proofed to be efficient on parallel systems by Habermann \cite{Habermann72}.
S{\"u}{\ss} et al. \cite{suss04} compared several parallel programming patterns
for simple sorting algorithms using \textit{OpenMP}. However the main algorithm
used for evaluation in this work was a form of quicksort and compared against a
\textit{posix} thread implementation instead of \textit{MPI}.
In \cite{elnashar11} Elnashar et al. conducted a research on the performance
of parallel \textit{MPI} implementations of sorting algorithms. The work focused
on windows based cluster and did not include a comparison to other parallel
programming paradigms.

\FloatBarrier
\section{Background}
\label{sec:background}

\subsection{Odd-Even Transposition Algorithm}
\label{subsec:oddEvenSortAlgo}
The odd-even transposition sorting algorithm was originally developed for parallel
processors with local interconnections and is derived from bubble sort.
Given the numbers $x_0, x_1, \dots, x_{n-1}$ bubble sort compares in the first
step the pair of numbers $x_0$ and $x_1$ and moves the larger number to $x_1$.
In the next step $x_1$ and $x_2$ are compared against each other and the larger
one is moved to $x_2$ and so on. This is repeated until the largest number reaches
the position $x_{n-1}$ which completes the first phase. Figure \ref{fig:bubble_sort}
shows this procedure for a list of numbers in which six is placed at the end of the
list. In order to bring the second largest number at the right place the second phase
repeats this procedure for numbers $x_0$ to $x_{n-2}$. In this way the larger
numbers `bubble` towards the end.

With an input size of $n$ numbers the algorithm performs $n-1$ \textit{compare-and-exchange}
operations in the first phase, $n-2$ in the second phase and so on. Therefore
the final complexity is $O(n^2)$ resulting from the total number of operations \cite{Wilkinson04}

\begin{equation*}
  Number of operations: \sum_{i=1}^{n-1} i = \frac{n(n-1)}{2}
\end{equation*}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{bubble_sort.eps}
    \caption{Steps of the first phase in bubble sort. The largest number is moved
            to the very end of the list.}
    \label{fig:bubble_sort}
\end{figure}

Bubble sort as described by this algorithm is purely sequential. However it can
be observed that a phase can be executed safely behind a prior phase as long
as it does not overtake the preceding phase. Therefore bubble sort can benefit
from pipelining techniques.

This leads to a variation of bubble sort which alternates between an \textit{odd} phase
and an \textit{even} phase. This algorithm is known as \textit{odd-even transposition sort}.
In an even phase all even indexed numbers are compared with their right neighbors.
Similar an odd phase does the same for all odd indexed numbers.

Executed sequentially this algorithm has no advantage over regular bubble sort
and shares the same complexity class. However if the compare-and-exchange operations
within a phase are performed all in parallel the complexity is reduced to $O(N)$.
Figure \ref{fig:odd_even_sort} illustrates how this algorithm sorts a list of
six numbers in parallel. \cite{Wilkinson04}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{odd_even_sort.eps}
    \caption{Odd-even transposition sort sorting six numbers.}
    \label{fig:odd_even_sort}
\end{figure}

\subsection{Bitonic Sequence}
\label{subsec:bitonicSeq}

The basis for the bitonic sorting algorithm are bitonic sequences which
have various properties that are used in the algorithm. A monotonic sequence
is a sequence in which all values are in either increasing or decreasing order.
A bitonic sequence is a sequence which consists of two consecutive monotonic
sequences one increasing and the other decreasing. Therefore a bitonic sequence
reaches a single maximum or minimum. Given the values $x_0, x_1, \dots, x_{n-1}$
an example for a bitonic sequence would be

\begin{equation*}
  x_0 < x_1 < x_2, \dots, x_{i-1} < x_i > x_{i+1} > x_{i+2} > x_{i+3}, \dots, x_{n-1}
\end{equation*}

for any value $i (0 \leq i < n)$. A sequence is also bitonic if the previous condition
is fulfilled after a cyclic rotation in either direction is performed on the
sequence. It is obvious that a bitonic sequence can be created by sorting one
sequence in increasing order and concatenate it with a second sequence which
has been sorted to be in decreasing order. This observation leads to a special
characteristic of bitonic sequences. If a compare-exchange-operation on $x_i$
with $x_{i+n/2}$ is performed for every $i (0 \leq i < n/2)$ with $n$ numbers
in the sequence, two new bitonic sequences are created and the numbers in the
first sequence are all less than in the second one. If this procedure is repeated
until every bitonic sequence consists of only one element, the whole sequence
will be sorted as illustrated in figure \ref{fig:bitonic_split}. Therefore any
bitonic sequence can be sorted by applying a certain number of these consecutive
\textit{bitonic-split} operations. \cite{Wilkinson04}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{bitonic_split.eps}
    \caption{Split of bitonic sequence until it is sorted.}
    \label{fig:bitonic_split}
\end{figure}

\subsection{Sorting Networks}
\label{subsec:sortingNetworks}

A sorting network is a comparison network that reorders any arbitrary sequence
of numbers into a monotonically increasing sequence and therefore sorts its
input. Comparison networks are composed of two basic elements, comparators
and wires.

A \textit{comparator} is a device with two inputs $x$ and $y$ and two corresponding
outputs $x'$ and $y'$. It reorders its two inputs so that $x' = min(x,y)$ and
$y' = max(x,y)$ or vice versa. \textit{Wires} are lanes that transport values
across it and connect several comparators together. Wires in which the
inital sequence of numbers enters the network are called \textit{input wires}.
Likewise wires that produce the final sequence are called \textit{output wires}.
Figure \ref{fig:sorting_network} shows a sorting network for four numbers and
how it reorders the sequence into a monotonically increasing one. \cite{Cormen09}

\begin{figure}[h]
  \centering
  \includegraphics[width=1.0\textwidth]{sorting_network.eps}
  \caption{Sorting network for four numbers. \cite{WikiSortingNetwork}}
  \label{fig:sorting_network}
\end{figure}

Sorting networks have two important properties, size and depth. \textit{Size}
is the total number of comparators in the entire network while \textit{depth}
describes the largest number of comaparators any value can encounter while
traveling through the network. Networks with minimum size or depth are called
\textit{size-optimal} and \textit{depth-optimal}. The sorting network shown
in figure \ref{fig:sorting_network} is optimal with regard to both properties.
Another interesting property is the so called \textit{zero-one principle}
which states that any network that sorts an arbitrary sequence of zeros and ones
sorts any given set of input numbers. \cite{Cormen09}

Although sorting networks are very simple constructs, it is very hard to reason
about optimal networks for a given number of inputs. Table \ref{tab:optimalNetworks}
shows the properties of all known optimal networks at the time of writing. The
first eight networks were proven in Knuth's \textit{Art of Computer Programming}
\cite{Knuth98}. The remaining were proven only recently in 2014 and it is not expected
that further advances in this regard will be made. \cite{Bundala14}

\begin{table}[h]
\centering
\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline
\textbf{n}                 & 1 & 2 & 3 & 4 & 5 & 6  & 7  & 8  & 9  & 10 & 11 & 12 & 13 & 14 & 15 & 16 & 17 \\ \hline
\textbf{Depth}             & 0 & 1 & 3 & 3 & 5 & 5  & 6  & 6  & 7  & 7  & 8  & 8  & 9  & 9  & 9  & 9  & 10 \\ \hline
\textbf{Size, upper bound} & 0 & 1 & 3 & 5 & 9 & 12 & 16 & 19 & 25 & 29 & 35 & 39 & 45 & 51 & 56 & 60 & 71 \\ \hline
\textbf{Size, lower bound} & 0 & 1 & 3 & 5 & 9 & 12 & 16 & 19 & 25 & 29 & 33 & 37 & 41 & 45 & 49 & 53 & 58 \\ \hline
\end{tabular}
\bigskip
\caption{Known optimal sorting networks. \cite{WikiSortingNetwork}}
\label{tab:optimalNetworks}
\end{table}

\subsection{Bitonic Sorting Algorithm}
\label{subsec:bitonicSortAlgo}

The \textit{bitonic sorting} algorithm is a method for constructing a sorting
network for an arbitrary input size of numbers by using  properties of bitonic
sequences.
As described in section \ref{subsec:bitonicSeq} any bitonic sequence can be
transformed into a monotonic, and therefore sorted sequence, by applying bitonic
split operations until every sequence consists of only one element. The only
step missing is how to get from an arbitrary unsorted sequence to a bitonic one.

A bitonic sorter for two numbers is simply a single compare-exchange-operation.
Starting with an unordered sequence we can sort every adjacent pair of numbers
alternating between descending and ascending order. Groups of four numbers will
then consist of two monotonic sequences, one ascending and the other descending.
This groups form therefore bitonic sequences. Sorting one group with bitonic
splits in ascending order and the next group in descending order creates two
new monotonic sequences. These sequences again form a bitonic sequence which
is doubled in size. This procedure can be recursively applied until a single
bitonic sequence is created which can be transormed into the final sorted one.

This procedure can be described by a sorting network. Figure \ref{fig:sorting_network}
shows a network for bitonic sorting with eight inputs. It can be be observed
that all different stages can be executed in parallel. This makes the algorithm
suitable for implementation on multicore systems. \cite{batcher68}

\begin{figure}[h]
  \centering
  \includegraphics[width=1.0\textwidth]{bitonic_sort.eps}
  \caption{Bitonic sorting network for eight numbers. \cite{lang17}}
  \label{fig:sorting_network}
\end{figure}

\FloatBarrier
\section{Implementation}
\label{sec:implementation}

\subsection{Odd-Even Transposition Sort OpenMP}
\label{subsec:oddEvenSortImpl}

For the \textit{OpenMP} implementation of the odd-even transposition sort
a straightforward serial implementation of the algorithm, as seen in listing
\ref{lst:oddEvenSerial}, was used as a starting point.

The implementation uses two different variables to track if an exchange operation
took place in the last iteration and which phase the algorithm is currently in.
The algorithm firstly enters a loop that is executed as long as there are exchange
operations performed. An additional check on the \textit{phase} variable is necessary
for the loop condition to ensure that always a pair of odd and even phases has been executed.
Inside the loop the \textit{exchange} variable is set to \textit{false} and
either a \textit{odd} or \textit{even} phase is executed. To determine which
one should be executed the initial loop counter is initialized with the
\textit{phase} variable which either points at the first or second element of
the array. In every iteration the current element is compared with the right neighbor
and the loop counter is advanced by two. If an exchange between the two elements
occurred \textit{exchange} is reset to \textit{true}. In the last step the current
phase is swapped.

\begin{lstlisting}[language=C, frame=single,
caption={Serial implementation of the odd-even transposition algorithm.},
label=lst:oddEvenSerial,
basicstyle=\scriptsize,
captionpos=b]
void odd_even_sort_serial(int *a, unsigned n) {
  bool exchange = true;
  unsigned phase = 0;

  while(exchange || phase) {
    exchange = false;
    for (unsigned i = phase; i < n - 1; i += 2) {
      if (compare_exchange(&a[i], &a[i + 1]))
        exchange = true;
    }
    phase = (phase == 0) ? 1 : 0;
  }
}
\end{lstlisting}

The inner \textit{for} loop can be fully run in parallel since every iteration is
completely independent from all other ones and executes a single
\textit{compare-and-exchange} operation. Therefore for an \textit{OpenMP} implementation
of the algorithm it is sufficient to annotate the \textit{for} loop with a single
\textit{omp parallel for} directive as seen in listing \ref{lst:oddEvenOmp1}.
Each thread can potentially write to the \textit{exchange} variable. But since
every thread will write the same value a synchronization is not necessary.

In terms of parallelism and workload balancing this is already ideal for this
algorithm. Each iteration is divided evenly among all processing units and in
each iteration every unit is active without idle time. However this implementation
is not without flaws. Since the complete \textit{for} loop is repeated itself a new
thread-group is created and destroyed each time.

\enlargethispage{\baselineskip}
\enlargethispage{\baselineskip}
\begin{lstlisting}[language=C, frame=single,
caption={First implementation of the OpenMP odd-even transposition algorithm.},
label=lst:oddEvenOmp1,
basicstyle=\scriptsize,
captionpos=b]
#pragma omp parallel for
for (unsigned i = phase; i < n - 1; i += 2) {
  if (compare_exchange(&a[i], &a[i + 1]))
    exchange = true;
}
\end{lstlisting}

It would be preferable to avoid the overhead of this repeated thread-group creation.
One way to achieve this is to push the thread-group creation outside of the
\textit{while} loop. This idea is illustrated in listing \ref{lst:oddEvenOmp2}.
An \textit{omp parallel} directive starts a thread-group. This thread-group enters
the \textit{while} loop and execute a phase in parallel as directed by
\textit{omp for}. To ensure that the phase is swapped only once, a \textit{omp single}
is employed.

While on the first look this implementation looks promising it can lead to
issues. It is possible that a thread sets the \textit{exchange} variable to
\textit{false} before other threads have read it. In this case a single thread
is waiting at the barrier of the \textit{for} loop while all other threads break out
to the implicit barrier at the end of the thread-group boundary. This results in
a deadlock situation. For this implementation to work, explicit synchronization
constructs are needed.

\begin{lstlisting}[language=C, frame=single,
caption={Attempt to reduce the overhead of the first OpenMP implementation.},
label=lst:oddEvenOmp2,
basicstyle=\scriptsize,
captionpos=b]
#pragma omp parallel
{
  while(exchange || phase) {
    exchange = false;
    #pragma omp for
    //Execute phase in parallel
    #pragma omp single
    //Swap phase
  }
}
\end{lstlisting}

A better solution to get rid of the overhead created by the thread-group creation
is shown in listing \ref{lst:oddEvenOmp3}. The loop which previously executed
both phases depending on the \textit{phase} variable is split up into two dedicated
loops. This eliminates the need to switch phases in each iteration. To solve the
possible deadlock scenario as described earlier an explicit barrier is inserted
before the \textit{exchange} variable is reset. In this way the thread creation
overhead can be avoided with minimal additional synchronization.

\enlargethispage{\baselineskip}
\begin{lstlisting}[language=C, frame=single,
caption={Final structure of the OpenMP implementation for the odd-even transposition algorithm.},
label=lst:oddEvenOmp3,
basicstyle=\scriptsize,
captionpos=b]
#pragma omp parallel
{
while(exchange) {
    #pragma omp barrier
    exchange = false;
    #pragma omp for
    //Execute even phase
    #pragma omp for
    //Execute odd phase
  }
}
\end{lstlisting}

\subsection{Odd-Even Transposition Sort MPI}

Like the \textit{OpenMP} implementation the \textit{MPI} version of the
odd-even transposition sort starts by allocating and initializing the initial
array. However while the master process of the started process group has to allocate
enough memory to hold the entire number array the slave processes only have
to reserve space for $\frac{N}{P}$ elements with $N$ being the total numbers
to sort and $P$ the number of started processes.

After every process allocated memory for its part of the array, the master process
distributes the numbers to sort equally with the \textit{MPI\_Scatter} function.
This function sends a contiguous blocks of elements to every process. Once a
process has received its block it sorts it sequentially with a regular
odd-even transposition sort. Since this algorithm has a complexity of $O(N^2)$
this step can be done $P^2$ times faster as in the complete serial case. After
this step every process has a partial block of the whole array which is sorted.

The remaining step is to combine all blocks back into the original array and ensure
that all elements are in sorted order. One possible approach would be to exchange
the minimum and maximum values at all block boundaries with each neighbor processes
and inserting them at the right position. The algorithm would then stop like the
original algorithm when there are no further changes. While it works and would
be closest to the parallel \textit{OpenMP} version it is highly ineffective.

A much more efficient way is to perform an odd-even sort on host instead of
element level. The relevant code part for this is shown in listing \ref{lst:oddEvenMPI}.
The basic idea is that neighbor processes exchange their entire block of elements.
Each process merges the two blocks together and the lower numbered process keeps
the lower half of the new block while the higher numbered process keeps the
upper half. In the even phase even numbered processors communicate with the
next odd numbered process and in the odd phase, odd numbered processes
communicate with the previous even numbered process. These two phases are
repeated $P$ times after which all blocks are guaranteed to be correctly merged.
In the final step the master process collects all blocks of the array with a
\textit{MPI\_Gather} operation.

\begin{lstlisting}[language=C, frame=single,
caption={Adapted odd-even sort to operate on host level.},
label=lst:oddEvenMPI,
basicstyle=\scriptsize,
captionpos=b]
for(int i = 0; i < num_proc; ++i) {
  if(i % 2 == 0) { //Even phase
    if(rank % 2 == 0) { //Even host
      if(rank < num_proc - 1) exchange_with_next(array, buffer, ...);
    }
    else { //Odd host
      if(rank > 0) exchange_with_previous(array, buffer, ...);
    }
  }
  else { //Odd phase
    if(rank % 2 == 0) { //Even host
      if(rank > 0) exchange_with_previous(array, buffer, ...);
    }
    else { //Odd host
      if(rank < num_proc - 1) exchange_with_next(array, buffer, ...);
    }
  }
}
\end{lstlisting}

\subsection{Bitonic-Sort OpenMP}
\label{subsec:bitonicSortImplOmp}

Like for the odd-even sorting the \textit{OpenMP} version of the bitonic-sort algorithm
is based on the serial implementation. The serial algorithm consists of only two
different functions \textit{bitonic\_sort} and \textit{bitonic\_merge}. Execution
starts with a single call to the bitonic\_sort function which is given in listing
\ref{lst:bitonicSort}. It takes as input a pointer to the array, the length of
the array and a direction in which the array should be sorted. In every function
call two recursive calls to bitonic\_sort are made with one half of the array respectively.
The lower half is called with an ascending direction while the upper half gets
a descending direction as input. This way the array is split up until the parts
reach a size of just two elements. Afterwards the functions return and bitonic\_merge
is called with the original array and direction.

\begin{lstlisting}[language=C, frame=single,
caption={Bitonic sort function.},
label=lst:bitonicSort,
basicstyle=\scriptsize,
captionpos=b]
void bitonic_sort(int* lo, unsigned n, bool dir) {
       if (n > 1) {
           int m=n/2;
           bitonic_sort(lo, m, ASCENDING);
           bitonic_sort(lo + m, m, DESCENDING);
           bitonic_merge(lo, n, dir);
       }
}
\end{lstlisting}

The bitonic\_merge function is depicted in listing \ref{lst:bitonicMerge}. As
the name suggests this function is responsible for merging two bitonic sequences
into one larger one. It does so by applying a bitonic split operation on the
array and calling the bitonic\_merge recursively on the two new bitonic sequences
that were created by the split operation. This is repeated until a sequence
with only two elements is reached, in which case the split operation is just
a single compare-exchange operation. Starting from this two element sequences
larger sequences are merged together until the end result is a single monotonic
sequence.

\begin{lstlisting}[language=C, frame=single,
caption={Bitonic merge function.},
label=lst:bitonicMerge,
basicstyle=\scriptsize,
captionpos=b]
void bitonic_merge(int* lo, unsigned n, bool dir) {
        if(n > 1) {
            int m=n/2;
            for (int i = 0; i <  m; i++) {
              compare_exchange(lo + i, lo + i + m, dir);
            }
            bitonic_merge(lo, m, dir);
            bitonic_merge(lo+m, m, dir);
        }
}
\end{lstlisting}

To parallelize this implementation of the algorithm using \textit{OpenMP} it would
be possible to just apply a \textit{parallel for} pragma to the \textit{for}
loop in bitonic\_merge. This would parallelize the bitonic split operation which
is completely safe to do. However doing so is very inefficient. For every split
operation a new thread group would be created even if the split only performs
a single compare-exchange operation.

In this case it is much more efficient to apply task parallelism to the
bitonic\_sort function  as shown in listing \ref{lst:bitonicSortOmp}. Since
the array gets always split up into a lower and a upper half which can be
processed independently, the recursive function calls are suitable to be
executed in parallel. For this an \textit{omp task} pragma is applied to each
of them. A merge operation can only be performed if both halves of the array
have been processed. To ensure this an \textit{omp taskwait} is applied. To take
full advantage of the machines resources \textit{OMP\_NESTED} must be set to
\textit{true} and the recursion level was limited by setting
\textit{OMP\_MAX\_ACTIVE\_LEVELS}.

\begin{lstlisting}[language=C, frame=single,
caption={Modifications to the \textit{bitonic\_sort} funtion for the OpenMP version.},
label=lst:bitonicSortOmp,
basicstyle=\scriptsize,
captionpos=b]
#pragma omp parallel
#pragma omp single nowait
{
  #pragma omp task
  bitonic_sort(lo, m, ASCENDING);

  #pragma omp task
  bitonic_sort(lo + m, m, DESCENDING);

  #pragma omp taskwait
  bitonic_merge(lo, n, dir);
}
\end{lstlisting}

Even tough task parallelism is more efficient than the initial approach,
performance measurements have shown that limiting the recursion level alone is not enough to avoid all overheads. While thread creation is
limited, tasks are still too fine grained. For this reason the implementation
was altered as shown in listing \ref{lst:bitonicSortOmpCutoff}. A new \textit{cutoff}
variable was introduced to limit the parallel level. Once the current array
size exceeds the set cutoff the serial implementation is called instead to avoid
the overhead of task creation.

\begin{lstlisting}[language=C, frame=single,
caption={Task parallel OpenMP implementation with cutoff.},
label=lst:bitonicSortOmpCutoff,
basicstyle=\scriptsize,
captionpos=b]
if(n > cutoff) {
  #pragma omp parallel num_threads(2)
  #pragma omp single nowait
  {
    #pragma omp task
    bitonic_sort_parallel(lo, m, cutoff, ASCENDING);
    #pragma omp task
    bitonic_sort_parallel(lo + m, m, cutoff, DESCENDING);
  }
}
else {
  bitonic_sort(lo, m, ASCENDING);
  bitonic_sort(lo + m, m, DESCENDING);
}
\end{lstlisting}

\subsection{Bitonic-Sort MPI}
\label{subsec:bitonicSortImplMPI}

Similar to the \textit{MPI} version of the odd-even sorting algorithm the \textit{MPI} bitonic
sort firstly allocates memory for the array to be sorted. Slave processes again
only need to allocate space for $\frac{N}{P}$ elements with $N$ being the total numbers
to sort and $P$ the number of started processes. After this every process sorts
its local array by using the serial version of the bitonic sorting algorithm.
Afterwards all host processes exchange their complete arrays in a standard
bitonic pattern as described in \cite{ercal17}. The implementation of this
procedure can be seen in listing \ref{lst:bitonicSortMPI}.

To exchange the arrays of the processes in a bitonic fashion every process rank
is treated as a binary number and the processes are thought of as aligned on a
hypercube of dimension $D = log_2(P)$. In every step a process either calls
\textit{compare\_high($j$)} or \textit{compare\_low($j$)}. These functions
exchange the array with the process whose binary rank only differs at the $j^{th}$
bit position and either keeps the lower or higher half of the merged array.
Which of these two functions should be called is dependent on a \textit{window-id}
which consists of the $D-i$ most significant bits of the rank number. This id
is used to determine if a process is considered odd or even. If an process is
even and its $j^{th}$ bit is zero or if it is odd and has a $j^{th}$ bit of one
it calls the \textit{compare\_low($j$)} function. Otherwise a \textit{compare\_high($j$)}
is performed. In this way an exchange pattern is created which transforms the
sequence in the first $\frac{D^2-D}{2}$ steps into a single bitonic sequence
and the last $D$ steps finally sort this sequence with a regular bitonic pattern.

\begin{lstlisting}[language=C, frame=single,
caption={Bitonic sort on host process level.},
label=lst:bitonicSortMPI,
basicstyle=\scriptsize,
captionpos=b]
int id, target_rank, dim = ((unsigned) log2(num_proc));
bool j_bit;

for(int i = 1; i <= dim; i++) {
  id = rank >> i;
  for(int j = i-1; j >= 0; j--) {
    j_bit = rank & (1 << j);
    target_rank = rank ^ (1 << j);
    if(((id % 2 == 0) && !j_bit) || ((id % 2 != 0) && j_bit))  {
      compare_low(array, buffer, size_per_proc, target_rank);
    }
    else {
      compare_high(array, buffer, size_per_proc, target_rank);
    }
  }
}
\end{lstlisting}

\FloatBarrier
\section{Evaluation}
\label{sec:evaluation}

\subsection{Performance}
\label{subsec:performance}

All measurements for performance evaluation were gathered on machine equipped
with one \textit{Intel Core i7 920} quad-core CPU clocked at 2.66 gigahertz and
6 gigabyte of memory. All algoritms were compiled with the \textit{GNU C Compiler}
in version 7.2 at optimization level \textit{O2}. For all measurements the average
of five independent runs was taken.

Figure \ref{fig:odd_even_performance} shows a performance comparison for all
three odd-even-transposition sort implementations and three different input
sizes. From the performance of the serial implementation it can be seen
that the algorithm has a complexity of $O(N^2)$ as the runtime increases
roughly four times with a doubled input size. The \textit{OpenMP} version performs
well up to a thread-group size of 4 which equals the physical cores of the
test machine. On large input sizes additional threads produce only synchronization
overhead and there was no benefit of the \textit{HyperThreading} feature.
On average the OpenMP version reached a speedup of $2.9$ for four cores.

In comparison the \textit{MPI} implementation performed way better. Because of the complexity
of the serial algorithm the \textit{MPI} version runs in a case of superlinear speedup since
one array with size $\frac{N}{4}$ can be sorted $16$ times faster than one with
$N$ elements. On average the \textit{MPI} version reached a speedup of $14.8$ on four CPU cores.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{odd_even_performance.eps}
    \caption{Performance comparison of odd-even transposition sort implementations.}
    \label{fig:odd_even_performance}
\end{figure}

In figure \ref{fig:bitonic_performance} the performance comparison for all
different \textit{bitonic} sort implementations is depicted. On the first look
it can be seen that \textit{bitonic} sort has a lower complexity class.
Therefore the performance gap between the \textit{OpenMP} and \textit{MPI}
implementations is much smaller compared to the \textit{odd-even transposition} sort.
Another difference is that all implementations benefit from the activated
\textit{HyperThreading} feature in the case of eight threads. The \textit{OpenMP}
version performed well on all input sizes and thread counts. On average the speedup
on four CPU cores was $3.2$. The \textit{MPI} version performed even better
and still could achieve a small superlinear speedup of $4.03$.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{bitonic_performance.eps}
    \caption{Performance comparison of bitonic sort implementations.}
    \label{fig:bitonic_performance}
\end{figure}

The previous performance comparisons have shown that the \textit{MPI}
versions of both algorithms performed significantly better compared
to the \textit{OpenMP} variants. As described in section \ref{sec:implementation}
the \textit{MPI} implementations follow a strategy in which the array
is distributed evenly to each host followed by a serial sort and merge
of all subarrays. To make a direct comparison between those \textit{APIs}
possible the same strategy was also implemented using \textit{OpenMP}.
The results of a direct comparison between those two can be seen in figure
\ref{fig:omp_mpi_performance}. Both \textit{APIs} performed nearly the same.
This indicates that the performance improvement indeed comes from the
choice of parallelizaton strategy rather than \textit{MPI} itself.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{mpi_omp_merge_comparison.eps}
    \caption{Performance comparison of MPI and OpenMP}
    \label{fig:omp_mpi_performance}
\end{figure}


\subsection{Memory Consumption}
\label{subsec:memory}

Both the odd-even transposition sort and the bitonic-sort are inplace sorting algorithms in their serial
implementation form. Therefore the serial implementations
of both algorithms only need to allocate memory for $N$ elements to sort an array
of the same size. Both \textit{OpenMP} versions of the algorithms were implemented by
just annotating code parts of the original serial implementations with pragmas
and do not need any additional memory.

The \textit{MPI} versions however needed a significant amount of additional memory.
The master process allocates space for $N$ elements while for all slave processes
it is sufficient to reserve memory for $\frac{N}{P}$ elements, with $P$ being
the number of total processes. In addition every process needs an additional
buffer with size $\frac{N}{P}$ elements. This buffer is used as temporary storage
for the array merge which is needed in both algorithms. Hence the master process
allocates space for $N+\frac{N}{P}$ elements and every slave needs $2\frac{N}{P}$
elements of allocated memory.

\FloatBarrier
\section{Conclusion}
\label{sec:conclusion}

This paper compared different parallel \textit{OpenMP} and \textit{MPI}
implementations of sorting algorithms. The \textit{odd-even transposition} and
\textit{bitonic} sort were chosen for this purpose due to their different properties.
Section \ref{sec:evaluation} provided the results of several evaluations performed
on the different implemented sorting algorithms. It was shown that in every test
case the \textit{MPI} version was able to outperform \textit{OpenMP}. It was
also shown that this was due to the parallelization strategy that was chosen
for the \textit{MPI} variants. However this implementations needed additional memory.

With regard to the implementation effort it is not possible to clearly rank one
of the two parallelization techniques over the other. In the case of \textit{OpenMP}
it was possible to simply extend the existing serial implementations with
pragma directives and benefit from performance improvements. But in many cases
it was not immediately clear where performance is lost. On the other hand
\textit{MPI} is much more explicit and has fewer pitfalls but requires a complete
redesign of the algorithm from the beginning.

\newpage

% References
\bibliographystyle{dbis}
\bibliography{Sorting-openmp-mpi-Stiegernigg.bib}

\end{document}
